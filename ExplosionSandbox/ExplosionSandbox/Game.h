#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <vector>
#include "ScreenManager.h"

class PhysicalObject;

class Game {
public: 
	Game();
	void init(int windowWidth,int windowHeight);
	void update();
	void draw();
	void checkEvents();

	~Game();

private:
	void initSdl();
	SDL_Window * window;
	SDL_GLContext context;
	
	ScreenManager * screenManager;
};


#endif