#ifndef VECTOR_H
#define VECTOR_H
#include "Point2.h"
#include <cmath>



class Vector 
{
public:

	
	Vector(Point2<float> vel): velocity(vel) {  }
	Vector(float x,float y): velocity(  Point2<float>(x,y)  ) { }
	Vector(): velocity(0.0f,0.0f) { }


	Point2<float> & getVelocity() 
	{
		return velocity;
	}

	Vector operator*(float m)
	{
		return Vector(this->getVelocity().getX() *m,this->getVelocity().getY() * m);
	}

	Vector operator-(Vector & vector)
	{
		return Vector( Point2<float>( this->getVelocity().getX() - vector.getVelocity().getX(),this->getVelocity().getY() - vector.getVelocity().getY()) );
}

	Vector operator+(Vector & vector) 
	{
		return Vector( Point2<float>( this->getVelocity().getX() + vector.getVelocity().getX(),this->getVelocity().getY() + vector.getVelocity().getY()) );
	}

	Vector getPerp()
	{
		return Vector(this->getVelocity().getY(),-this->getVelocity().getX() );
	}

	float operator[](unsigned int i)
	{
		if( i <=0 ) return velocity.getX();
		return velocity.getY();
	}

	static float dot(Vector & v1,Vector v2)
	{
		return v1[0]*v2[0] + v1[1]*v2[1];
	}


	void operator+=(Vector & vector)
	{
		velocity.setX( this->velocity.getX() + vector.getVelocity().getX()  );
		velocity.setY( this->velocity.getY() + vector.getVelocity().getY() );
	}

	float magnitude()
	{
		return sqrt ( velocity.getX()*velocity.getX() + velocity.getY()*velocity.getY() );
	}

private:

	Point2<float> velocity; 
};

#endif
