#pragma once
#ifndef SCREENMAINMENU_H
#define SCREENMAINMENU_H
#include "screen.h"
#include <GL/glew.h>

class ScreenMainMenu : public Screen {
public:

	ScreenMainMenu(int newID, std::string screenName) {
		ID = newID;
		name = screenName;
	}

	~ScreenMainMenu(void)
	{
	}

	void init() { }

	void display() {
		glColor3f(1.0f,1.0f,1.0f);
		glBegin(GL_POLYGON);
			glVertex2f(0, 0);
			glVertex2f(0.5, 0);
			glVertex2f(0, 0.5);
		glEnd();
	}
	
	void update() { ;}

	void handleEvents() { ;}
private:

};
#endif

