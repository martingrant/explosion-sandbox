#pragma once
#ifndef SCREENGAME_H
#define SCREENGAME_H

#include "Screen.h"
#include "PhysicalObject.h"
#include "Point4.h"
#include <vector> //DO NOT ADD using namespace std;
#include "ExplosiveRay.h"
#include "DragDrop.h"
class PhysicalObject;
class DragDrop;

class ScreenGame : public Screen {
public:
	ScreenGame(int newID, std::string screenName);
	~ScreenGame(void);

	void init();
	void display();
	void update();
	void handleEvents();

	static Point4<float> GAME_BOUNDS2;
	std::vector<ExplosiveRay*>& getExplosiveRays() { return explosiveRays; }
	float getGravity() { return GRAVITY2; }
	float getCoR() { return CoR; }
	float getCoSF() { return CoSF; }
	float getCoKF() { return CoKF; }

private:
	static const float GRAVITY2;
	static const float CoR;
	static const float CoSF;
	static const float CoKF;
	bool exploded;
	void checkBounds(PhysicalObject& p); //checks that the object is within the game bounds
	
	void checkRays();

	std::vector<PhysicalObject*> objects;
	std::vector<ExplosiveRay *> explosiveRays;

	DragDrop * dragDrop;
};
#endif
