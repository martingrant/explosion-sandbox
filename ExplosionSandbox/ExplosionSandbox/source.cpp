#include "Game.h"
#include <iostream>

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

int main(int argc,char * argv[])
{	
	const int FPS = 30;

	double frameTime = 1.0 / (double)FPS;
    frameTime = frameTime * 1000;
    int milliFrameTime = (int)frameTime;
    int sleep;

	Uint32 lastTick;
    Uint32 currentTick;

	Game game;

	game.init(800,800);

	bool run = true;
	SDL_Event e;

	while(run)
	{
		lastTick = SDL_GetTicks();

		while( SDL_PollEvent(&e) )
		{
			if( e.type == SDL_QUIT )
				run = false;
		}

		game.update();
		game.draw();

		currentTick = SDL_GetTicks();
		
		sleep = milliFrameTime - (currentTick - lastTick);
		if(sleep < 0) { sleep = 0; }
		SDL_Delay(sleep);

		game.checkEvents();
	}

	return 0;
}