#ifndef POINT2_H
#define POINT2_H

template<typename type>
class Point2 // simply a pair of points of <type> 
{
public:
	Point2(type ix ,type iy): x(ix),y(iy) {}
	Point2() {x = y = 0;}

	type & operator[](unsigned int i)
	{
		if( i == 0 )
			return x;
		return y;
	}
	
	void operator+=(Point2<type> & rhs)
	{
		x += rhs.getX();
		y += rhs.getY();
	}

	Point2<type> operator+(Point2<type> & rhs)
	{
		return Point2<float>(this->getX() +rhs.getX(),this->getY() + rhs.getY() );
	}

	Point2<type> operator-(Point2<type> & rhs)
	{
		return Point2<float>(this->getX() - rhs.getX(),this->getY() - rhs.getY() );
	}


	type  getX() { return x;}
	type  getY() { return y;}
	void setX(const type t) { x = t; }
	void setY(const type t) { y = t; }
private:
	type x,y;

};


#endif