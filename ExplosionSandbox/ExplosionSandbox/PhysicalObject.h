#ifndef PHYSICAL_OBJECT_H
#define PHYSICAL_OBJECT_H
#include "Vector.h"
#include <GL\glew.h>
#include <iostream>
#include <vector>
#include "Game.h"
#include "ScreenGame.h"
#include "DragDrop.h"
#include "PhysicalObject.h"

class Game;

class PhysicalObject{
public:
	//takes in the X,Y cordinate as well as the Width and hight in the form of Point2 and the mass
	PhysicalObject(Point2<float> pos,Point2<float> dim, float m);
	PhysicalObject(Point2<float> pos,Point2<float> dim, Point4<GLubyte> col);

	/*PhysicalObject(const PhysicalObject * copy); //Copy Constructor*/

	void applyForce( Vector v); // adds Vector v to the resulatantVelocity

	void move();
	void draw();

	bool operator!=(PhysicalObject & rhs) { return this->index != rhs.index; }
	bool operator==(PhysicalObject & rhs) { return this->index == rhs.index; }
	Point2<float> &getPosition();
	Point2<float> &getDimensions();
	Vector& getResultantVelocity();
	float getMass();

	void explode(ScreenGame &g,int amount);

	void applyFriction( PhysicalObject *obj, float staticFriction, float kineticFriction );

	bool checkCollision(PhysicalObject *p);

	bool gravityCheck( const std::vector<PhysicalObject*> * listOfAllObjects,ScreenGame* g);//this checks which blocks are affected by gravity
	void removeOverlap( PhysicalObject *higher, PhysicalObject *lower );
	PhysicalObject centerOfMassCheck( PhysicalObject *higher, PhysicalObject *lower, float move );

	bool & isAffectedByGravity() { return affectedByGravity; }

	void setPosition(Point2<float> newPosition) { position = newPosition; }
	void toggleSelected(bool toggle) { selected = toggle; }
	bool isSelected() { return selected; }
	static bool &  isSomthingSelected() { return somthingSelected; }
private:
	bool affectedByGravity;
	
	
	static int count;
	int index;

	Vector resultantVelocity;
	Point2<float> position;
	Point2<float> dimensions;

	Point4<GLubyte> colours;
	float mass;
	
	bool selected;
	static bool somthingSelected;
	
};

#endif