#include "ScreenGame.h"

const float ScreenGame::GRAVITY2 = 9.8f*0.1;
const float ScreenGame::CoR = 0.1f;		// Coefficent of restitution
const float ScreenGame::CoSF = 0.7f;		// Coefficent of Static Friction
const float ScreenGame::CoKF = 0.65f;		// Coefficent of Kinetic Friction
Point4<float> ScreenGame::GAME_BOUNDS2 = Point4<float>(0.0f,0.0f,4000.0f,4000.0f);

ScreenGame::ScreenGame(int newID, std::string screenName) {
	explosiveRays = std::vector<ExplosiveRay*>();
	objects = std::vector<PhysicalObject*>();

	ID = newID;
	name = screenName;
	exploded= false;

	dragDrop = new DragDrop();
}


ScreenGame::~ScreenGame(void)
{

}

void ScreenGame::init()
{

	float mass = 3;
	objects.push_back( new PhysicalObject(Point2<float>(1500.0f,200.0f),Point2<float>(200.0f,200.0f),Point4<GLubyte>(255,0,0,255)) ); // bomb
	objects.push_back( new PhysicalObject(Point2<float>(1000.0f,0.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1000.0f,200.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1000.0f,400.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1000.0f,600.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1000.0f,800.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1900.0f,0.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1900.0f,200.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1900.0f,400.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1900.0f,600.0f),Point2<float>(400.0f,200.0f),mass) );
	objects.push_back( new PhysicalObject(Point2<float>(1900.0f,800.0f),Point2<float>(400.0f,200.0f),mass) );



}

void ScreenGame::display() {

	for(int i = 0; i < objects.size(); i++)
	{
		objects[i]->draw();
	}
	
	for(int i =0; i < explosiveRays.size(); i++)
	{
		explosiveRays[i]->draw();
	}

}



void ScreenGame::checkRays()
{
	
	for(int i =0; i < explosiveRays.size(); i++)
	{
		bool brk = false;
		explosiveRays[i]->update();
		for(int j = 0; j < objects.size(); j++)
		{

			Point2<float> pos = Point2<float>( ((explosiveRays[i]->getPosition().getX() + 1)*2000),
											((explosiveRays[i]->getPosition().getY() +1 )*2000));
		
			if( objects[j]->checkCollision(
				new PhysicalObject(pos - Point2<float>(1.0f,1.0f), Point2<float>(2.0f,2.0f), 1.0f) ) )
			{
				Point2<float> vel = Point2<float>( ((explosiveRays[i]->getVeclocity().getVelocity().getX())*2000)*0.1,
													((explosiveRays[i]->getVeclocity().getVelocity().getY())*2000)*0.1);
				
				objects[j]->applyForce( Vector(vel) );
				explosiveRays.erase( explosiveRays.begin() + i );
				
				checkRays();
				brk = true;
				break; // have to break outa for loop
			}

		}

		if( brk ) break;
	}

}

void ScreenGame::update() {
	if( objects.size() == 0 ) return; // if no objects


	checkRays();

	for(int i =0; i < explosiveRays.size(); i++)
	{
		bool brk = false;
		explosiveRays[i]->update();
		for(int j = 0; j < objects.size(); j++)
		{

			Point2<float> pos = Point2<float>( ((explosiveRays[i]->getPosition().getX() + 1)*2000),
											((explosiveRays[i]->getPosition().getY()+1 )*2000));
		
			if( objects[j]->checkCollision(
				new PhysicalObject(pos - Point2<float>(1.0f,1.0f), Point2<float>(2.0f,2.0f), 1.0f) ) )
			{
				explosiveRays.erase( explosiveRays.begin() + i );
				brk = true;
				break; // have to break outa for loop
			}

		}

		if( brk ) break;
	}

	
	dragDrop->update(*objects[0]);

	for( int i = 1; i < objects.size(); i++)
	{
		objects[i]->gravityCheck(&objects,this); //check which objects are affected by gravity 

		if( objects[i]->isAffectedByGravity() ) {
			if ( objects[i]->isSelected() == false ) // object not selected by mouse
				objects[i]->applyForce( Vector(Point2<float>(0.0f,-GRAVITY2) ) ); //apply gravity only to the objects that are affected by it
		}
		objects[i]->move(); // move objects by their resultant velocity
		checkBounds(*objects[i]); //check objects are with in the bounds of the game 

		dragDrop->update(*objects[i]);
	}

	
}

void ScreenGame::handleEvents() {
	Uint8 *keys = SDL_GetKeyboardState(NULL);

	if ( keys[SDL_SCANCODE_1] ) objects[0]->applyForce( Vector(Point2<float>(0.0f,50.0f) ));

	if ( keys[SDL_SCANCODE_SPACE] ) if(!exploded){ objects[0]->explode( *this,100); exploded = true; }

	if ( keys[SDL_SCANCODE_RETURN] )
	{
		for(int i = 0; i < explosiveRays.size(); i++)
		{
			delete explosiveRays[i];
		}
		explosiveRays.clear();
		exploded = false;
	}

	if ( keys[SDL_SCANCODE_R] )
	{
		for(int i = 0; i < objects.size(); i++)
		{
		 	delete objects[i];
		}
		objects.clear();
		init();
	}

	int x, y;
	SDL_GetMouseState(&x, &y);
	x = x*5; y=4000-(y*5);
	if ( keys[SDL_SCANCODE_N] ) {
		PhysicalObject * newBox = NULL;
		newBox = new PhysicalObject(Point2<float>(x,y),Point2<float>(400.0f,200.0f),10);
		newBox->toggleSelected(true);
		objects.push_back( newBox );
		dragDrop->update(*newBox);
	}

}

void ScreenGame::checkBounds(PhysicalObject & p)
{
	if( p.getPosition().getX() < ScreenGame::GAME_BOUNDS2.getX() )
	{
		if( p.getResultantVelocity().getVelocity().getX() < 0.0f)
			p.getResultantVelocity().getVelocity().setX( -p.getResultantVelocity().getVelocity().getX() * getCoR());
		
	}

	else if( p.getPosition().getX() +p.getDimensions().getX() > ScreenGame::GAME_BOUNDS2.getX() + ScreenGame::GAME_BOUNDS2.getZ())
	{
		if( p.getResultantVelocity().getVelocity().getX() > 0.0f )
		{
			p.getResultantVelocity().getVelocity().setX( -p.getResultantVelocity().getVelocity().getX() * getCoR() );
		}
	}

	if( p.getPosition().getY() + p.getDimensions().getY() > ScreenGame::GAME_BOUNDS2.getY() + ScreenGame::GAME_BOUNDS2.getW() )
	{
		if( p.getResultantVelocity().getVelocity().getY() > 0.0f )
		{
			p.getResultantVelocity().getVelocity().setY( -p.getResultantVelocity().getVelocity().getY() * getCoR()) ;

		}

	}

	else if( p.getPosition().getY() < ScreenGame::GAME_BOUNDS2.getY() )
	{
		if( p.getResultantVelocity().getVelocity().getY() < 0.0f )
		{
			p.getResultantVelocity().getVelocity().setY( -(float)(int)(p.getResultantVelocity().getVelocity().getY()) * getCoR() );
		}
	}
}
