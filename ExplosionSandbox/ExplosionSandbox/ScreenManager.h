#pragma once
#ifndef SCREENMANAGER_H
#define SCREENMANAGER_H

#include "Screen.h"

// Provides switching of different screens, maintains an array of screens and pointers to individual screens
// Can switch via setCurrentScreen(int screenID), check ScreenManager.cpp's constructor for IDs.
// To create a new screen, create a new class inheriting from Screen.h, add a pointer to it within ScreenManager.h as well as updating
// this class' constuctor and setCurrentScreen() method. Initial Screen is currently being set in Game.cpp's init method.


class ScreenManager {
public:
	ScreenManager(void);
	~ScreenManager(void);

	void init(void);
	void display(void);
	void update(void);
	void handleEvents(void);

	void setCurrentScreen(int screenID);

private:
	friend class Screen;
	friend class ScreenGame;
	friend class ScreenMainMenu;

	Screen * currentScreen;
	Screen * mainmenu;
	Screen * game;

	Screen * screenList[];
};
#endif
