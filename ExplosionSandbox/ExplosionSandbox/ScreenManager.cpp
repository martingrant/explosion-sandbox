#include "ScreenManager.h"
#include "ScreenMainMenu.h"
#include "ScreenGame.h"


ScreenManager::ScreenManager(void) {
	mainmenu = new ScreenMainMenu(1, "MainMenu");
	game = new ScreenGame(2, "Game");

	screenList[0] = mainmenu;
	screenList[1] = game;
}

ScreenManager::~ScreenManager(void) {

}

void ScreenManager::init(void) {
	setCurrentScreen(2);
	currentScreen->init();
}

void ScreenManager::display(void) {
	currentScreen->display();
}

void ScreenManager::update(void) {
	currentScreen->update();
}

void ScreenManager::handleEvents(void) {
	currentScreen->handleEvents();
}

void ScreenManager::setCurrentScreen(int screenID) {

	for(int i=0; i<2; i++)
		if ( screenList[i]->getID() == screenID ) {
			currentScreen = screenList[i];
			std::cout << "Set Screen to: " << screenList[i]->getName() << std::endl;
		}
}