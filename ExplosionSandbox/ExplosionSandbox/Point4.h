#ifndef POINT4_H
#define POINT4_H


template<typename type>
class Point4{
public:

	Point4(type ix,type iy,type iz,type iw): x(ix),y(iy),z(iz),w(iw) {} 


	type & operator[](unsigned int i)
	{
		if( i == 0)
			return x;
		else if( i == 1 )
			return y;
		else if( i == 2)
			return z;
		return w;
	}


	type  getX() { return x;} 
	type  getY(){ return y;}
	type  getZ() { return z;}
	type  getW() { return w;}

	void setX(const type t) { x =t; }
	void setY(const type t) { y =t; }
	void setZ(const type t) { z =t; }
	void setW(const type t) { w =t; }
private:
	type x,y,z,w;
};


#endif