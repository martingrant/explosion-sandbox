#ifndef PHYSICS_H
#define PHYSICS_H

#include "Vector.h"
#include <cmath>

class Physics{
public:
	static float intensity(float distance)
	{
		return 1/(distance*distance);
	}


	static float force(float m,float a)
	{
		return m*a;
	}

	static float acceleration(float f,float m)
	{
		return m/f;
	}

	static float friction(float u, float n)
	{
		return u*n;
	}
	
//	static bool checkGravity(PhysicalObject & object,std::vector<
};

#endif