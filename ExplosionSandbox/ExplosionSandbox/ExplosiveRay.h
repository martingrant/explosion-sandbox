#ifndef EXPLOSIVERAY_H
#define EXPLOSIVERAY_H
#include "Vector.h"
#include <GL\glew.h>
#include <iostream>

class ExplosiveRay
{
public:

	ExplosiveRay(Point2<float> pos, Vector vel):velocity(vel),startingPosition(pos)
	{
		position = pos;

	}

	void draw()
	{
		float x = position.getX();
		float y = position.getY();
		float startX = startingPosition.getX();
		float startY = startingPosition.getY();

		glColor3f(1.0f,0.0f,0.0f);
		glLineWidth(6.0f);
		glBegin(GL_LINES);
			glVertex2f(x,y);
			glVertex2f(startX,startY);
		glEnd();
	}

	Point2<float> getPosition() { return position;}
	Vector  getVeclocity() { return velocity;}

	void update()
	{
		position += velocity.getVelocity();
	}

private:
	Vector  velocity;
	Point2<float> startingPosition;
	Point2<float> position;

};

#endif