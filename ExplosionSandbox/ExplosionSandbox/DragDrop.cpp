#include "DragDrop.h"

DragDrop::DragDrop()
{
}

DragDrop::~DragDrop()
{
}

void DragDrop::draw()
{
}

void DragDrop::update(PhysicalObject &p) 
{
	int x, y;
	SDL_GetMouseState(&x, &y);
	x = x*5; y=4000-(y*5);

	if(SDL_GetMouseState(NULL, NULL) && SDL_BUTTON(1)) { // left mouse button
		if ( (x > p.getPosition().getX()) && ( x < p.getPosition().getX() + p.getDimensions().getX() ) && ( y > p.getPosition().getY() ) && ( y < p.getPosition().getY() + p.getDimensions().getY() )) 
			{
				if( !PhysicalObject::isSomthingSelected() )
				{
					p.toggleSelected(true);
					PhysicalObject::isSomthingSelected() = true;
					return;
				}
			}	
	} 
	else {
		
		p.toggleSelected(false);
		PhysicalObject::isSomthingSelected() = false;
	}

	if ( p.isSelected() == true ) 
	{
		p.setPosition(Point2<float>(x-p.getDimensions().getX()/2, y-p.getDimensions().getY()/2));
	}
}