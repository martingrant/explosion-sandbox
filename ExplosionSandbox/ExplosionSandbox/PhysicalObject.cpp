#include "PhysicalObject.h"
#include "ExplosiveRay.h"
#include "Physics.h"
#include <cmath>

int PhysicalObject::count = 0;
bool PhysicalObject::somthingSelected = false;


PhysicalObject::PhysicalObject(Point2<float> pos,Point2<float> dim,Point4<GLubyte> cols): position(pos), dimensions(dim), 
	resultantVelocity ( Vector( Point2<float>(0.0f,0.0f) ) ),mass(0),colours(cols)	
{
	index = count++;
}


PhysicalObject::PhysicalObject(Point2<float> pos,Point2<float> dim, float m): position(pos), dimensions(dim), 
	resultantVelocity ( Vector( Point2<float>(0.0f,0.0f) ) ),mass(m),colours(255,255,255,255)	
{
	index = count++;
}

void PhysicalObject::applyForce( Vector v)
{
	resultantVelocity += v*mass;
}
/*
PhysicalObject::PhysicalObject(const PhysicalObject * cpy): resultantVelocity(cpy->resultantVelocity)
{
	this->dimensions = cpy->dimensions;
	this->mass = cpy->mass;
	this->position = cpy->position;
	
	index = count++;
}*/

bool PhysicalObject::checkCollision(PhysicalObject * p) // will need to be changed
{
	if( this->position.getX() + this->dimensions.getX() < p->getPosition().getX() )
		return false;
	if( this->position.getX() > p->getPosition().getX() +p->getDimensions().getX() )
		return false;
	if( this->position.getY() > p->getPosition().getY() + p->getDimensions().getY() )
		return false;
	if( this->position.getY() + this->dimensions.getY() < p->getPosition().getY() )
		return false;

	return true;
}

void PhysicalObject::move()
{
	position.setX( position.getX() + resultantVelocity.getVelocity().getX() );
	position.setY( position.getY() + resultantVelocity.getVelocity().getY() );
}

void PhysicalObject::draw()
{
	float x = ((position.getX())/ 2000)-1;
	float y = ((position.getY())/ 2000)-1;
	float width = ((dimensions.getX())/2000); 
	float height = ((dimensions.getY())/2000);

	glColor4ub(colours[0],colours[1],colours[2],colours[3] );
	glBegin(GL_POLYGON);
		glVertex2f(x,y);
		glVertex2f(x,y + height);
		glVertex2f(x+ width,y + height);
		glVertex2f(x + width,y);
	glEnd();
}

Point2<float>& PhysicalObject::getPosition()
{
	return position;
}

Vector& PhysicalObject::getResultantVelocity()
{
	return resultantVelocity;
}

float PhysicalObject::getMass() 
{
	return mass; 
}

Point2<float> &PhysicalObject::getDimensions()
{
	return dimensions;
}

/* Corrects collision that occur inbetween frames */
void PhysicalObject::removeOverlap( PhysicalObject *higher, PhysicalObject *lower )
{
		// Distance object has travelled inside another object
	float boxOverlap;

		// Take the lower objects y from the higher objects y to find the overlap
	boxOverlap = ( lower->getPosition().getY() + lower->getDimensions().getY() ) - higher->getPosition().getY();
		// Move the object with the higher y coord by the overlap
	higher->getPosition().setY( higher->getPosition().getY() + boxOverlap );

}

PhysicalObject PhysicalObject::centerOfMassCheck( PhysicalObject *higher, PhysicalObject *lower, float move )
{
		// is the higher object's center of gravity to the right of the lower object
	if ( higher->getPosition().getX() + ( higher->getDimensions().getX()/2 ) < lower->getPosition().getX() + lower->getDimensions().getX()
		&& higher->getPosition().getX() +  (higher->getDimensions().getX()/2 ) < lower->getPosition().getX() )
	{
			// move left
		higher->applyForce(Vector( Point2<float> ( -move, 0.0f) ));
	}
		// is the higher object's center of gravity to the right of the lower object
	else
		if ( higher->getPosition().getX() + (higher->getDimensions().getX()/2) > lower->getPosition().getX() + lower->getDimensions().getX()
		&& higher->getPosition().getX() + (higher->getDimensions().getX()/2) > lower->getPosition().getX() )
	{
			// move right
		higher->applyForce(Vector( Point2<float> ( move, 0.0f) ));
	}

	return *higher;
}

	/* Calculates if the force acting on the objectFriction  */
void PhysicalObject::applyFriction( PhysicalObject *obj, float staticFriction, float kineticFriction )
{
		// if object dx is less than Static friction => dx = 0 the object does not move
	if ( this->resultantVelocity.getVelocity().getX() >= -staticFriction && this->resultantVelocity.getVelocity().getX() <= staticFriction )
	{
		this->resultantVelocity.getVelocity().setX(0.0f);
	}
		// if object dx is greater than 0 => dx - Kinetic friction (object moving right)
	if ( this->resultantVelocity.getVelocity().getX() > 0 )
	{
		applyForce(Point2 <float>(-kineticFriction, 0.0f));
	}
		// if object dx is less than 0 => dx + Kinetic friction (object moving left)
	else if ( this-> resultantVelocity.getVelocity().getX() < 0 )
	{
		applyForce(Point2 <float>(kineticFriction, 0.0f));
	}
}

bool PhysicalObject::gravityCheck(const std::vector<PhysicalObject*> *obj, ScreenGame * g) 
{

	float N = g->getGravity();
	float CosF = g->getCoSF() * N;
	float CokF = g->getCoKF() * N;

	if(  this->position.getY() <=  g->GAME_BOUNDS2.getY() ) // if colides with the bottom
	{
		affectedByGravity = false;

		this->applyFriction( this, CosF, CokF );
		
		return affectedByGravity;
	}

	for(int i = 1; i < obj->size(); i++)  // for each object in game
	{

		if( this != (*obj)[i] && this->checkCollision( (*obj)[i] ) ) // if this collides with the object
		{
			//std::cout << "box [ " << this->index << " ]  collided with box " << "[ " << i << " ] " << std::endl;

			// Check which objects y coord is greater
			if ( this->getPosition().getY() < (*obj)[i]->getPosition().getY() ) // if this object y  is less than other object y
			{	
				removeOverlap( (*obj)[i], this );
				centerOfMassCheck( (*obj)[i], this, N );
				(*obj)[i]->applyFriction( this, CosF, CokF );

			}
			else	// if this object is more than other object
			{
				removeOverlap( this, (*obj)[i] );
				centerOfMassCheck( this, (*obj)[i], N );
				this->applyFriction( this, CosF, CokF );
			}

			if ( (*obj)[i]->resultantVelocity.getVelocity().getY() > 0 )
			{
				this->resultantVelocity.getVelocity().setY( - this->resultantVelocity.getVelocity().getY() * g->getCoR() );
			}

			if( !(*obj)[i]->isAffectedByGravity() ) // and that object is touching the ground
 			{
 				affectedByGravity = false;
				return affectedByGravity;
			}

		}

	}

	affectedByGravity = true;
	return affectedByGravity;
}

void PhysicalObject::explode(ScreenGame &g,int amount)
{
	float x = ((this->position.getX()+this->getDimensions().getX()/2)/2000.0f) - 1;
	float y = ((this->position.getY()+this->getDimensions().getY()/2)/2000.0f) - 1;
	float twopi = 2 * 3.1415926535;
	float step = twopi/amount;
	for(float i= 0.0f; i < twopi; i+= step)
	{

		g.getExplosiveRays().push_back( new ExplosiveRay( Point2<float>( x +(  0.1* cos(i)), y+ (0.1*sin(i)) ),

			Vector(Point2<float>(  (0.1* +cos( i)), ( 0.1*sin(i) ) ))));

		// Point2<float>(cos( i* this->getPosition().getX()),sin(i)*this->getPosition().getY())
	}
}
