//#pragma once
#ifndef DRAGDROP_H
#define DRAGDROP_H
#include "PhysicalObject.h"
class PhysicalObject;
class DragDrop
{
public:

	DragDrop();
	~DragDrop(void);

	void draw();
	void update(PhysicalObject &p);

private:

};
#endif
