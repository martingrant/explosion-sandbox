#include "Game.h"
#include "ExplosiveRay.h"

Game::Game()
{
	screenManager = new ScreenManager();
} 

void Game::init(int w,int h)
{
	initSdl();
	window = SDL_CreateWindow("Explosion Sandbox",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED , w ,h, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	context = SDL_GL_CreateContext(window);

	screenManager->init();
	
	SDL_GL_SetSwapInterval(1);
}

void Game::checkEvents()
{
	screenManager->handleEvents();
}

void Game::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	screenManager->display();

	SDL_GL_SwapWindow(window);
}

void Game::initSdl()
{
	if( SDL_Init(SDL_INIT_VIDEO) )
		exit(1);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,1);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,8);
}

void Game::update()
{
	screenManager->update();

}

Game::~Game()
{
	SDL_DestroyWindow(window);
	SDL_Quit();
}



