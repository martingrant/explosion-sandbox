#ifndef POINT3_H
#define POINT3_H


template<typename type>
class Point3{
public:
	Point3(type ix,type iy ,type iz): x(ix),y(iy),z(iz) {	}

	type & operator[](unsigned int i)
	{
		if( i == 0)
			return x;
		else if( i == 1) 
			return y;

		return z;
	}

	type  getX() { return x; }
	type  getY() { return y; }
	type  getZ() { return z; }


	void setX(const type  t) { x = t;}
	void setY(const type  t) { y = t;}
	void setZ(const type  t) { z = t;}

private:
	type x,y,z;

};


#endif 