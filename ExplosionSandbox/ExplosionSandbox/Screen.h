#pragma once
#ifndef SCREEN_H
#define SCREEN_H

#include "Point4.h"
#include <string>

// Abstract Screen class used for inheritance and switching in ScreenManager

class Screen {
public:

	Screen() {
	
	}

	virtual ~Screen(void)
	{
	}

	virtual void init() = 0;
	virtual void display() = 0;
	virtual void update() = 0;
	virtual void handleEvents() = 0;

	int getID() { return ID; }
	std::string getName() { return name; }

protected:
	int ID;
	std::string name;

};
#endif
